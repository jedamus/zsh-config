# zsh-config

This is my zsh configuration.

file structure is: ~/.zshrc ~/.env ~/zsh/config ~/zsh/functions ~/bin/zshprompt.py

The new prompt in zshprompt.py is called when a .git-subdirectory is found.
